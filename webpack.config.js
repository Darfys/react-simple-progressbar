const path = require("path");

let configDev = {
  entry: "./example/index.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
      },
    ]
  },
  devServer: {
    static: "./example/",
    port: 3000,
    historyApiFallback: true,
    hot: true,
    open: true
  },
  output: {
    path: path.resolve("build"),
    filename: 'index.js',
  },
}

let configProd = {
  entry: "./src/index.js",
  mode: "production",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
      },
    ]
  },
  resolve: { extensions: ["*", ".js", ".jsx"] },
  output: {
    path: path.resolve("build"),
    filename: 'index.js',
    libraryTarget: 'umd'
  },
  externals: {
    react: "react"
  }
}

module.exports = (env, argv) => {

  if (argv.mode === 'development') {
    return configDev;
  }

  if (argv.mode === 'production') {
    return configProd;
  }

}
