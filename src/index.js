import React from "react"

const styles = {
  container: {
    height: 6,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#d9d9d9",
    position: "fixed",
    zIndex: 10,
    width: "100%",
    top: 0,
    left: 0,
  },
  indicator: {
    backgroundColor: "#2972f0",
    height: "100%",
  }
}

const ProgressBar = ({value, style}) => {
  return (
    <div style={{...styles.container, ...style?.container}}>
      <div style={{...styles.indicator, ...style?.indicator, width: `${value}%`}}></div>
    </div>
  )
}

export default ProgressBar