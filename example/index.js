import React from "react";
import ReactDOM from "react-dom";
import ProgressBar from "../src/index";

ReactDOM.render(<ProgressBar value={50}/>, document.getElementById("app"));